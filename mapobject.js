// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    
function mapObject(obj, cb) {
    let map = [];
    for(let key in obj){
        if (obj.hasOwnProperty(key)) {
            map[key] = cb(obj[key], key, obj);
          }
       }
    return map;
    
}

module.exports = mapObject;