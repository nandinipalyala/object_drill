// Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs

    function pairs(obj) {
        let result = [];
        for(let [key,value] of Object.entries(obj)){
            result.push([key, value])
        }
        return result;
    }
    
    module.exports = pairs;