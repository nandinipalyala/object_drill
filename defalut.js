
// Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults

    function defaults(obj, defaultProps) {
        for (let [key, value] of Object.entries(defaultProps)) {
            if (obj[key] === undefined) {
              obj[key] = value;
            }
          }
        
          return obj;
    }
    
    module.exports = defaults;