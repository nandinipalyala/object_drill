// Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values

function values(obj) {
    let result = [];
    for(let key of Object.values(obj)){
        result.push(key)
    }
    return result;
    
}
module.exports = values;